package com.entersekt.EnterSekt;

import com.entersekt.models.FileAtrribute;
import com.entersekt.models.FileDetail;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributeView;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
@CrossOrigin
@RequestMapping(value = "/systemcheck")
public class SystemCheck {
    
    @GetMapping("/")
    public String listDir(){
        return "Welcome to Directory List Application";
    }

    @GetMapping("/listfiles")
    public List<FileDetail> listFilesUsingFilesList(@RequestParam(value="name", defaultValue="") String dir) throws IOException {
        try {
            String path = "/";
            List<Path> dirs = Files.walk(Paths.get(path+dir), 1)
                    .filter(Files::isDirectory)
                    .collect(Collectors.toList());
            List<FileDetail> filelist = new ArrayList<FileDetail>();
            for (Path path2 : dirs) {
                FileChannel fileChannel = FileChannel.open(path2);
                BasicFileAttributeView basicView = Files.getFileAttributeView(path2, BasicFileAttributeView.class);
                filelist.add(new FileDetail(String.valueOf(path2), String.valueOf(fileChannel.size()), new FileAtrribute(String.valueOf(basicView.readAttributes().lastModifiedTime()), String.valueOf(basicView.readAttributes().lastAccessTime()), String.valueOf(basicView.readAttributes().creationTime()))));
                //System.out.println(fileChannel.size());
            }
            return filelist;
        }catch(FileNotFoundException ex){
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "File Not Found", ex);
        }catch (IOException ex){
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "I/O Error!", ex);
        }catch (Exception ex){
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "Error Occured!!", ex);
        }
    }
}
