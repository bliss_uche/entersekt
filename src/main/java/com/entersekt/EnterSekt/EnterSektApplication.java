package com.entersekt.EnterSekt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EnterSektApplication {

	public static void main(String[] args) {
		SpringApplication.run(EnterSektApplication.class, args);
	}

}
