package com.entersekt.models;

import java.sql.DataTruncation;

public class FileDetail {
    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getFileSize() {
        return fileSize;
    }

    public void setFileSize(String fileSize) {
        this.fileSize = fileSize;
    }

    public FileAtrribute getFileAttribute() {
        return fileAttribute;
    }

    public void setFileAttribute(FileAtrribute fileAttribute) {
        this.fileAttribute = fileAttribute;
    }

    public FileDetail(String path, String size, FileAtrribute attribute){
        filePath = path;
        fileSize = size;
        fileAttribute = attribute;
    }

    public String filePath;
    public String fileSize;
    public FileAtrribute fileAttribute;
}
