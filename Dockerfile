FROM openjdk:8
VOLUME /tmp
ADD target/EnterSekt-0.0.1-SNAPSHOT-jar-with-dependencies.jar EnterSekt-0.0.1-SNAPSHOT-jar-with-dependencies.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","EnterSekt-0.0.1-SNAPSHOT-jar-with-dependencies.jar"]