### Getting Start

1. Compile the Application.

    `mvn clean install -DskipTests`
    
2. Start the Application.

   ```
    bash-3.2$ ./run.sh
   ```
    
    OR

   ```
    java -jar target/EnterSekt-0.0.1-SNAPSHOT-jar-with-dependencies.jar
   ``` 
    
3. Access route.    

   ```
   localhost:8080/systemcheck/listfiles?name=query